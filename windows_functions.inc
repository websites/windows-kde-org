<?php

function maintainerTableHeader()
{
    global $site_locale;
    startTranslation($site_locale);
    print "<h2><a name=\"maintainers\">" . i18n_var("Windows Package Maintainers") . "</a></h2>\n";
    print "<table style=\"width: 100%;\" class=\"table_box\" cellpadding=\"6\">\n";
    print "<tr>\n<th>" . i18n_var("Module") . "</th>\n<th>" . i18n_var("Maintainer (IRC Nick)") . "</th>\n<th>" . i18n_var("Comments") . "</th>\n</tr>\n";  
}

function maintainerTableEntry( $module, $maintainer, $comment )
{
    print "<tr>\n";
    print "<td valign=\"top\">" . $module . "</td>\n";
    print "<td valign=\"top\">" . $maintainer . "</td>\n";
    print "<td valign=\"top\">" . $comment . "</td>\n";
    print "</tr>\n";
}

function maintainerTableFooter()
{
    print "</table>\n";
}

// this is basically stolen from KDE-EDU function kde_edu_app_news

function read_news( $folder, $app_name, $items, &$dates, &$titles, &$apps, &$index )
{
  $news = new RDF();
  $rdf_pieces = $news->openRDF( $folder . "/news.rdf" );

  if(!$items)
  {
     $items = 5; // default
  }
  $rdf_items = count($rdf_pieces);
  if ($rdf_items > $items)
  {
     $rdf_items = $items;
  }

  //only open the file if it has something in it
  if ($rdf_items > 0)
  {
    if ($rdf_items < $items)
    {
      $rdf_items = $rdf_items - 1;
    }

    for($x=1;$x<=$rdf_items;$x++)
    {
      ereg("<title>(.*)</title>", $rdf_pieces[$x], $title);
      ereg("<date>(.*)</date>", $rdf_pieces[$x], $date);
      $printDate = $date[1];
      
      $index++;
      $dates[ $index ] = strtotime( $date[ 1 ] );
      $titles[ $index ] = "<a href=\"" . $folder . "/news.php#item" . ereg_replace( "[^a-zA-Z0-9]", "", $title[ 1 ] ) . "\">" . $title[ 1 ] . "</a>";
      $apps[ $index ] = $app_name;
    }
  }
}

// Sort and output top news

function kde_windows_news()
{
  global $site_locale;
  startTranslation($site_locale);
  
 print "<a href=\"rss.php\" title=\"RSS formatted news feed\" style=\"float:right\"><img src=\"pics/rss.png\" width=\"28\" height=\"16\" alt=\"RSS\"/></a>";
  print "<h2><a name=\"news\">" . i18n_var("Latest News") . "</a></h2>\n";
  print "<table style=\"width: 100%;\" class=\"table_box\" cellpadding=\"6\">\n";
  print "<tr>\n<th>" . i18n_var("Date") . "</th>\n<th>" . i18n_var("Headline") . "</th>\n</tr>\n";
//  . "\n<th>Application</th>\n</tr>\n";

  $index = 0;
  $dates = array();
  $titles = array();
  $apps = array();

  $num_main_rdf = 8;
  $num_app_rdf = 4;

  read_news ( ".", "", $num_main_rdf, $dates, $titles, $apps, $index );
//  read_news ( "installer", "Kdewin-Installer", $num_app_rdf, $dates, $titles, $apps, $index );
  
  array_multisort( $dates, SORT_DESC, $titles, $apps );

  $number = min( count( $dates ), 10 );
  $alternate =false;
  
  for( $index=0; $index<$number; $index++ )
  {
    $alternate = !$alternate;
    if ($alternate)
    {
      $color = "newsbox1";
    }
    else
    {
      $color = "newsbox2";
    }
    $printDate = date( "jS F Y", $dates[ $index ] );
    print "<tr>\n"
         ."<td valign=\"top\" class=\"$color\">" . (($printDate == $prevDate) ? "&nbsp;" : "<b>$printDate</b>") . "</td>\n"
	 ."<td valign=\"top\" class=\"$color\">" . $titles[ $index ] . "</td>\n"
//	 ."<td valign=\"top\" class=\"$color\">" . $apps[ $index ] . "</td>\n"
	 ."</tr>\n";
    $prevDate = $printDate;
  }
  print "</table>\n";
} 

?>