<?php
$page_title = "People behind KDE on Windows";
include ( "header.inc" );

function person($name, $text, $icon="empty.png")
{
    echo '<p style="clear:left">';
    echo ' <img width="50" vspace="8" hspace="8" src="pics/people/'.$icon.'" alt="'.$name.'" style="float:left" />';
    echo '<br/>';
    echo ' <b>'.$name.'</b>&nbsp;'. $text;
    echo '</p><br/>';
}
echo '<p>(Alphabetic ordering)';
person('Pedro Lopez-Cabanillas','implemented the Windows backend for KMid in 2010 - it works really good');
person('Stuart Dickson','from <a href="http://www.kogmbh.com/">KO GmbH</a> is working on the Windows port of Calligra.');
person('Casper van Donderen','ported <a href="http://gluon.gamingfreedom.org/">Gluon</a> to Windows.','caspar-van-donderen.jpg');
person('Ralf Habacker','(rhabacker) is the creator of the KDE on Windows Installer and is working on porting KDE since the times of KDE 1.','ralf-habacker.png');
person('Ilie Halip','added installer support for debug packages within the Google Summer of Code 2010 program','ilie-halip.png');
person('André Heinecke','works on the Windows port of the <a href="http://pim.kde.org">KDEPim</a> application suite.','andre-heinecke.png');
person('Andreas Holzammer','works on the WinCE port of the <a href="http://pim.kde.org">KDEPim</a> application suite.','andreas-holzammer.png');
person('Maurice Kalinowski','works on the Windows port of <a href="http://skrooge.org">scrooge</a>.','maurice-kalinowski.jpg');
person('Shane King','(shakes) works on the Windows port of Amarok and is supporting us from Down-Under.');
person('John Layt','');
person('Adrian Page','works on <a href="htp://krita.org">Krita</a> for Windows.');
person('Romain Pokrzywka','(kromain) - a senior software developer at KDAB - works on the <a href="http://pim.kde.org">KDEPim</a> application suite.
He held a <a href="http://www.youtube.com/watch?v=spPjWpcBN4o">KDE on windows talk</a> at Camp KDE 2010.','romain-pokrzywka.png');
person('Patrick von Reth','works on the integration of mingw-w64 and mingw-32 bit compilers into the emerge build system.','patrick-von-reth.png');
person('Carlo Segato','(_Brandon_) worked on several projects in KDE and ported both ktorrent and <a href="http://www.gmplib.org/">gmp</a> to windows.
He is currently the youngest member in the team.','csegato-80.jpg');
person('Wolfgang Rohdewald','increased the code quality of the python based emerge system');
person('Patrick Spendrin','(SaroEngels) studied Physics in Leipzig and works now as a Qt developer. 
He took over maintainership of the <a href="http://techbase.kde.org/Getting_Started/Build/KDE4/Windows/emerge">emerge</a> 
build tool from Holger Schr&ouml;der. He spend most of his time dedicated to KDE on Windows with bug tracking and user suppport. ','ps-sq-clr-80.png');
person('Jaroslaw Staniek','(jstaniek) develops multiplatform 
applications using C++ and <a href="http://www.trolltech.com/products/qt/">Trolltech\'s Qt</a>. 
In 2003 he started contributing to KDE by starting 
<a href="http://wiki.kde.org/tiki-index.php?page=KDElibs+for+win32">KDElibs for win32</a> project 
(formerly <a href="http://iidea.pl/~js/qkw/">Qt-KDE Wrapper</a>), which has been extended to the 
<a href="http://windows.kde.org">KDE on Windows</a> project. <a href="http://kexi-project.org/pjs.html">more info &raquo;</a>','jstaniek-dublin-80.jpg');
person('Bernhard Reiter','is Managing Direcor and owner of <a href="http://www.intevation.de">Intevation GmbH</a>. 
This year he donated the meeting rooms for the KDE on Windows meeting in Osnabrück.','bernhard-reiter.png');
person('Sascha Teichmann','contributed an extended dependency system to the emerge build system');
person('Constantin-Alexandru Tudorica','works on an <a href="http://www.google-melange.com/gsoc/project/google/gsoc2011/tudalex/20001">Attica based KDE installer</a> 
within the Google Summer of Code 2011 program.');

echo '<h1 style="clear:left">Non active members</h1>';
person('Christian Ehrlicher','works as a Qt and maintained packaging of all KDE on Windows packages so far. 
He fixed a lot of bugs and kept KDE on Windows running.');
person('Peter K&uuml;mmel','worked on the windows port of dbus.');
person('Holger Schr&ouml;der',
'started the native port of Qt 3 to the Windows platform. Later he started our 
<a href="http://techbase.kde.org/Getting_Started/Build/KDE4/Windows/emerge">emerge</a> build tool. 
He finished his studies in computer sciences and works as a Qt programmer now.');

echo '<h1 style="clear:left">Supporting members</h1>';

include ( "footer.inc" );
?>