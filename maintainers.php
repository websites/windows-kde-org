<?php
    $page_title = "Maintainers of KDE on Windows packages";
    include ( "header.inc" );
    maintainerTableHeader();
    
    maintainerTableEntry("digikam", "<a href=\"mailto:caulier.gilles@gmail.com\">Gilles Caulier</a>", "");
    maintainerTableEntry("kdebase-apps", "<b>core team</b>", "");
    maintainerTableEntry("kdebase-runtime", "<b>core team</b>", "");
    maintainerTableEntry("kdebase-workspace", "<i>unset</i>", "not ported");
    maintainerTableEntry("kdebindings", "<i>unset</i>", "not ported");
    maintainerTableEntry("kdevelop", "<i>unset</i>", "not ported");
    maintainerTableEntry("kdevplatform", "<i>unset</i>", "not ported");
    maintainerTableEntry("kdeedu", "Patrick Spendrin (SaroEngels)", "");
    maintainerTableEntry("kdegames", "<i>unset</i>", "");
    maintainerTableEntry("kdegraphics", "Patrick Spendrin (SaroEngels)", "");
    maintainerTableEntry("kdelibs", "<b>core team</b>", "");
    maintainerTableEntry("kdenetwork", "<i>unset</i>", "");
    maintainerTableEntry("kdepim", "Andre Heinecke (aheinecke)", "");
    maintainerTableEntry("kdepimlibs", "Andre Heinecke (aheinecke)", "");
    maintainerTableEntry("kdesdk", "<i>unset</i>", "");
    maintainerTableEntry("kdetoys", "<i>unset</i>", "");
    maintainerTableEntry("kdeutils", "Patrick Spendrin (SaroEngels)", "not complete yet");
    maintainerTableEntry("kdewebdev", "Paulo Moura Guedes (Mojo_risin)", "not complete yet");
    maintainerTableEntry("koffice", "Ananth (ananth123)", "not released");
    
    maintainerTableFooter();
?>    
<p>note: packages that are still '<i>unset</i>' will probably not be in best shape.
If you have the time to build a package at least once a week for a longer time, you can
apply for maintainership on our mailing list: <a href="https://mail.kde.org/mailman/listinfo/kde-windows">kde-windows</a></p>
<?php
    include ( "footer.inc" );
?>
