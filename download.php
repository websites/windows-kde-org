<?php
    $page_title = "Download Information";
    include ( "header.inc" );
?>
<p>The preferred way of installing KDE apps under Windows is by using the <strong>KDE-Installer</strong>.

<table><tr><td><a href="http://download.kde.org/stable/kdewin/installer/kdewin-installer-gui-latest.exe.mirrorlist"><img src="pics/download.png"</img></a></td><td valign=center>latest installer for immediate installation.</td></tr></table>

<p>Other options are: <ul>
<li style="margin-bottom:1em">See the <a href="http://techbase.kde.org/Projects/KDE_on_Windows/Installation#KDE_Installer_for_Windows">KDE-Installer Screenshots</a> for a quick installer overview or 
<li style="margin-bottom:1em">see the <a href="http://techbase.kde.org/Projects/KDE_on_Windows/Installation">detailed Installation Instructions</a> on <strong>KDE TechBase</strong></li>
</ul>
</p>
<?php
    include ( "footer.inc" );
?>