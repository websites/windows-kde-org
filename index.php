<?php
    $page_title = "Welcome to The KDE on Windows Initiative";
    include ( "header.inc" );
?>

<p>The KDE on Windows Initiative is an ongoing project to bring the KDE applications to MS Windows.</p>

<p>The preferred method for installing most KDE applications under Windows is the <strong>KDE-Installer</strong> (<a href="download.php">Download Information</a>).
However, the most recent version available from the installer is KDE 4.10.2.</p>

<p>Many KDE applications have since been ported to the more modern "KDE Frameworks / KF5" libraries, and some have started providing Windows installers.
Due to the more modular nature of KDE Frameworks, there is no single installer for these applications. Rather, please refer to the website of the application(s) you wish to install.</p>

<p>Alternatively, it is possible to <a href="https://techbase.kde.org/Getting_Started/Build/Windows/emerge">build a large variety of KDE applications from source</a>
on Windows. If you are interested in versions not packaged in a binary installer, or you want to help in bringing your favorite application to Windows, refer
<a href="http://techbase.kde.org/Projects/KDE_on_Windows">to our documenation on the KDE TechBase</a>.</p>

<p>For support from project members you may get in contact via the 
<a href="http://webchat.freenode.net/?channels=kde-windows&amp;uio=MTE9MTk117">instant WebChat client</a> 
or by joining the <a href="irc://irc.freenode.net/kde-windows">#kde-windows</a> IRC channel on freenode. 
Before contacting project members directly please take a look at the 
<a href="http://techbase.kde.org/Projects/KDE_on_Windows">Project Overview</a>. 
Your question may already be answered there. </p>
<p>You can join our mailing list <a href="http://mail.kde.org/mailman/listinfo/kde-windows">kde-windows(@)kde.org</a> 
for news and announcements.<br /></p>

<?php
    kde_windows_news();
    include ( "footer.inc" );
?>
