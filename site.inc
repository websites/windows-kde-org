<?php

// promote title to use
$site_title = "KDE Windows Initiative";
$site_logo = "../../media/images/kde.png";
$site_search = false;
$site_menus = 1;
$templatepath = "chihuahua/";

$site_external = true;
$name = "windows.kde.org Webmaster";
$mail = "holger@holgis.net;ps_ml@gmx.de";
$showedit = false;

$rss_feed_link = "/rss.php";
$rss_feed_title = "Latest KDE-Windows News";

$site_showkdeevdonatebutton = true;

$piwikSiteID = 3;
$piwikEnabled = true;

include( "windows_functions.inc" );
// some global variables
$kde_current_version = "4.5";
?>

