﻿<?php
include("handler.inc");
$handler = new Handler404();
$handler->add("/doc", "/development/document.php");
$handler->add("/development/debug.php", "http://techbase.kde.org/Development/Tools");
$handler->add("/development/port2kde4.php", "http://techbase.kde.org/Development/Tutorials/KDE4_Porting_Guide");
$handler->add("/development/ghns.php", "http://techbase.kde.org/Development/Tutorials/Introduction_to_Get_Hot_New_Stuff");
$handler->add("/development/knewstuff.php", "http://techbase.kde.org/Development/Tutorials/Introduction_to_Get_Hot_New_Stuff");
$handler->execute();
?>
